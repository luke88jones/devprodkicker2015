(function() {
	"use strict";
	angular.module("starter")
		.factory("Device", Device);

	Device.$inject = ["$q"];

	function Device(
		$q
	) {
		var db;
	    db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
	    db.transaction(function (tx) {
  			tx.executeSql('CREATE TABLE IF NOT EXISTS tblDevice (property, value)', [], function() {
  				console.log("created table");
  			}, function() {
  				console.log("Error creating table");
  			}
  			);
  		});

		var getQuery = "Select value From tblDevice where property = 'deviceId'";

		var insertQuery = "Insert INTO tblDevice (property, value) VALUES (?,?)"

		var getDeviceId = function() {
			return $q(function (resolve, reject) {
				db.transaction(function (tx) {
	  				tx.executeSql(getQuery, [], function(tx, results) {
	  					if (results.rows.length === 0) {
	  						resolve(null);
	  					}
	  					else {
	  						resolve(results.rows[0].value);
	  					}
	  				}, function(err) {
	  					console.log(err);
	  				});
	  			});
			});
		};

		var setDeviceId = function() {
			var deviceId =generateUUID();
			return $q(function(resolve, reject) {
				db.transaction(function (tx) {
	  				tx.executeSql(insertQuery, ["deviceId", deviceId], function(tx, results) {
	  					resolve(deviceId);
	  				});
	  			});
			})
		};

	    function generateUUID(){
	        var d = new Date().getTime();
	        var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
	            var r = (d + Math.random()*16)%16 | 0;
	            d = Math.floor(d/16);
	            return (c === "x" ? r : (r&0x7|0x8)).toString(16); });
	        return uuid;
	    }

		return {
			uuid: null,
			setUUID: function() {
				var that = this;
				return getDeviceId()
					.then(function(result) {
						if (!result) {
							setDeviceId()
								.then(function(deviceId) {
									that.uuid = deviceId;
								})
						} else {
							that.uuid = result;
						}
					});
			}
		}
	}
})();
