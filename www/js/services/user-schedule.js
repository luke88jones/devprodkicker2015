(function() {
	"use strict";
	angular.module("starter")
		.factory("UserSchedule", UserSchedule);

	UserSchedule.$inject = [
		"Agenda",
		"UserService"
	];

	function UserSchedule(
		Agenda,
		UserService
	) {
		function getBlankSchedule() {
			var blankSchedule = {};
			Agenda.agenda.forEach(function(agendaItem) {
				agendaItem.sessions.forEach(function(session) {
					blankSchedule[session.id] = {
						sessionId: session.id,
						time: agendaItem.startTime,
						title: session.title,
						attending: false,
						rating: null
					};
				});
			});

			return blankSchedule;
		}

		return {
			schedule: null,
			getBlankSchedule: getBlankSchedule,
			updateSessionAttendance: function(sessionId, attending) {
				UserService.user.schedule[sessionId].attending = attending;
				UserService.user.$save();
			},
			updateSessionRating: function(sessionId, rating) {
				UserService.user.schedule[sessionId].rating = rating;
				UserService.user.$save();
			}
		};
	}
})();