(function() {
	"use strict";
	angular.module("starter")
		.factory("UserService", UserService);

	UserService.$inject = [
		"$firebaseAuth",
		"$firebaseObject",
		"$q",
		"Device"
	];

	function UserService(
		$firebaseAuth,
		$firebaseObject,
		$q,
		Device
	) {
		var ref = new Firebase("https://glaring-heat-1328.firebaseio.com/userSchedules");

		function checkIfUserExists(userId, cb) {
		  ref.child(userId).once('value', function(snapshot) {
		    var exists = (snapshot.val() !== null);
		    cb(userId, exists);
		  });
		}

		return {
			user: null,
			auth: function() {
				return $firebaseAuth(ref).$authAnonymously();
			},
			getUser: function(userId) {
				var that = this;
				var currentUser = ref.child(userId);
				that.user = $firebaseObject(currentUser);

				return that.user.$loaded();
			}
		};
	}

})();