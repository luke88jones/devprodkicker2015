(function() {
  "use strict";
  angular.module("starter")
    .factory("Agenda", Agenda);

    function Agenda() {
      var agendaItems = [
        {
          id: 1,
          startTime: "08:45",
          sessions: [
            {
              id: 1,
              title: "Registration",
              speaker: null
            }
          ]
        },
        {
          id: 2,
          startTime: "09:30",
          sessions: [
            {
              id: 2,
              title: "Welcome & Business Update",
              speaker: "Jeremy Wardell"
            }
          ]

        },
        {
          id: 3,
          startTime: "09:40",
          sessions: [
            {
              id: 3,
              title: "Vista Update – Product Management & Engineering",
              speaker: "Jeremy Wardell & James Berry"
            }
          ]
        },
        {
          id: 4,
          startTime: "10:00",
          sessions: [
            {
              id: 4,
              title: "Roadmap Highlights",
              speaker: "Adnan Riaz / Alistair Eaton / Polly Roberts"
            }
          ]
        },
        {
          id: 5,
          startTime: "10:45",
          sessions: [
            {
              id: 5,
              title: "Technology Radar Intro",
              speaker: "Matt Tanner"
            }
          ]
        },
        {
          id: 6,
          startTime: "11:00",
          sessions: [
            {
              id: 6,
              title: "Networking",
              speaker: "Emma Dew / Sara Carter"
            }
          ]
        },
        {
          id: 7,
          startTime: "11:15",
          sessions: [
            {
              id: 7,
              title: "Coffee Break",
              speaker: null
            }
          ]
        },
        {
          id: 8,
          startTime: "11:30",
          sessions: [
            {
              id: 8,
              title: "UX / UI",
              speaker: "Christian Baverstock"
            }
          ]
        },
        {
          id: 9,
          startTime: "12:00",
          sessions: [
            {
              id: 9,
              title: "Product Ownership",
              speaker: "Andrea Cornish"
            },
            {
              id: 10,
              title: "DevOps",
              speaker: "Matt Tanner"
            }
          ]
        },
        {
          id: 10,
          startTime: "12:30",
          sessions: [
            {
              id: 11,
              title: "Lunch",
              speaker: null
            }
          ]
        },
        {
          id: 11,
          startTime: "13:15",
          sessions: [
            {
              id: 12,
              title: "Technology Radar Update",
              speaker: "Matt Tanner"
            }
          ]
        },
        {
          id: 12,
          startTime: "13:30",
          sessions: [
            {
              id: 13,
              title: "Mad, Sad, Glad",
              speaker: "David Heiland",
              subTitle: "What worked, what didn’t and what we can use for the coming year?"
            }
          ]
        },
        {
          id: 13,
          startTime: "14:15",
          sessions: [
            {
              id: 14,
              title: "SAFe Presentation",
              speaker: "David Putman, Agil8",
              subTitle: "Portfolio Layer Session"
            },
            {
              id: 15,
              title: "Marker Landscape",
              speaker: "Adnan Riaz / Alistair Eaton / Polly Roberts"
            },
            {
              id: 16,
              title: "Angular & Ionic Mobile Development",
              speaker: "Dave Collins, Luke Jones, Matt Levy"
            }
          ]
        },
        {
          id: 14,
          startTime: "15:00",
          sessions: [
            {
              id: 17,
              title: "Coffee Break",
              speaker: null
            }
          ]
        },
        {
          id: 15,
          startTime: "15:15",
          sessions: [
            {
              id: 18,
              title: "SAFe Presentation",
              speaker: "David Putman, Agile8",
              subTitle: "Program Layer Session"
            },
            {
              id: 19,
              title: "Lean Thinking",
              speaker: "John Armstrong-Prior"
            },
            {
              id: 20,
              title: "Angular & Ionic Mobile Development",
              speaker: "Dave Collins, Luke Jones, Matt Levy"
            }
          ]
        },
        {
          id: 16,
          startTime: "16:00",
          sessions: [
            {
              id: 21,
              title: "Coffee Break",
              speaker: null
            }
          ]
        },
        {
          id: 17,
          startTime: "16:15",
          sessions: [
            {
              id: 22,
              title: "SAFe Presentation",
              speaker: "David Putman, Agile8",
              subTitle: "Team Layer Session"
            },
            {
              id: 23,
              title: "Lean Thinking",
              speaker: "John Armstrong-Prior"
            },
            {
              id: 24,
              title: "Domain Driven Design",
              speaker: "Lukasz Kwiecinski"
            }
          ]
        },
        {
          id: 18,
          startTime: "17:15",
          sessions: [
            {
              id: 25,
              title: "Closing Session",
              speaker: null,
              subTitle: "including Technology Radar Roundup"
            }
          ]
        },
        {
          id: 19,
          startTime: "17:30",
          sessions: [
            {
              id: 26,
              title: "Depart",
              speaker: null
            }
          ]
        }
        // {
        //   id: 20,
        //   startTime: "17:31",
        //   sessions: [
        //     {
        //       id: 27,
        //       title: "PARTY!!!!",
        //       speaker: null
        //     }
        //   ]
        // }
      ];

      return {
        agenda: agendaItems,
        getAgendaItem: function(agendaItemId) {
          for (var i = 0; i < agendaItems.length; i++) {
            if (agendaItems[i].id == agendaItemId) {
              return agendaItems[i];
            }
          }
        }
      }
    }

})();
