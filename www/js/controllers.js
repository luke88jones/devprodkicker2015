angular.module('starter.controllers', [])

.controller('AgendaCtrl', ["$scope", "Agenda", "$state", function($scope, Agenda, $state) {
  $scope.agendaItems = Agenda.agenda;

  $scope.goToSchedule = function() {
    $state.go("tab.schedule");
  };
}])

.controller('ScheduleCtrl', ["$scope", "UserService", "$state", "UserSchedule", function($scope, UserService, $state, UserSchedule) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  $scope.$on('$ionicView.enter', function(e) {
    $scope.userSchedule = UserService.user.schedule;
  });

  $scope.goToAgenda = function() {
    $state.go("tab.agenda");
  };

  $scope.unattend = function(session) {
    session.attending = false;
    UserSchedule.updateSessionAttendance(session.sessionId, session.attending);
  };
}])

.controller("AgendaItemCtrl", ["$stateParams", "$scope", "Agenda", "UserService", "UserSchedule", function($stateParams, $scope, Agenda, UserService, UserSchedule) {
  $scope.agendaItem = Agenda.getAgendaItem($stateParams.id);
  $scope.userSchedule = UserService.user.schedule;


  $scope.agendaItem.sessions.forEach(function(session) {
      session.attending = $scope.userSchedule[session.id].attending;
      session.rating = $scope.userSchedule[session.id].rating;
  });

  $scope.updateSessionAttendance = function(session) {
    UserSchedule.updateSessionAttendance(session.id, session.attending);
  };

  $scope.updateSessionRating = function(session) {
    UserSchedule.updateSessionRating(session.id, session.rating);
  };

  $scope.clearRating = function(session) {
    session.rating = null;
    $scope.updateSessionRating(session);
  }
}]);
